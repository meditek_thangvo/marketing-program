
import React from 'react';

const ListComponent = (props) => {

    const {list, attribution} = props;
    return (
        <div className="list-content">    

                {list.map((item, idx)=> (
                    <div key={idx} style={{display: 'flex'}}>
                        <p className="text-index-list">{`${item.index}.`}</p>
                        <p className="text-item-list">{item.text}</p>
                    </div>
                ))}

        </div>
    );
}

export default ListComponent;

