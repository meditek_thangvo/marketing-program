

import React from 'react';

const ParagraphComponent = (props) => {

    const {content} = props;
    return (
        <div className="content-text">    
            <p className="">{content}</p>
        </div>
    );
}

export default ParagraphComponent;

