import React from "react";

const PullQuoteComponent = (props) => {
  const { text, attribution } = props.quoteObj;
  return (
    <div className="pull-quote-text">
      <p className="quote-text">{text}</p>
      <p className="attribution-text">{attribution}</p>
    </div>
  );
};

export default PullQuoteComponent;
