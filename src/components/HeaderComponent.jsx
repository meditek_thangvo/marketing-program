import React from "react";
import moment from "moment";

const HeaderComponent = (props) => {
  const { headerText, sourceText, byLineText, publicationDate } = props;
  return (
    <div className="header-content">
      <h1 style={{ fontWeight: 400 }}>{headerText}</h1>
      <div className="source-line">
        <p className="source-text">{`${byLineText},`}</p>
        <p className="byline">{sourceText}</p>
      </div>
      <div className="date-line">
        <p>{moment(publicationDate).format("dddd, DD MMMM YYYY HH:mm")}</p>
      </div>
      <hr style={{ marginTop: 0 }} />
    </div>
  );
};

export default HeaderComponent;
