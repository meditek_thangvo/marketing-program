
import React from 'react';

const ImageComponent = (props) => {

    console.log('props is', props);

    const {captionText, url} = props.image;
    return (
        <div className="image-content">    
           <img className="image" src={url}></img>
           <p className="image-caption">{captionText}</p>
        </div>
    );
}

export default ImageComponent;

