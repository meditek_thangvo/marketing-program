import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import HeaderComponent from "../HeaderComponent";
import moment from "moment";

Enzyme.configure({ adapter: new Adapter() });

describe("HeaderComponent", () => {
  it("should be show Header information", () => {
    const wrapper = shallow(
      <HeaderComponent
        headerText={"HEADER"}
        sourceText={"SOURCE TEXT"}
        byLineText={"BY LINE TEXT"}
        publicationDate={new Date()}
      />
    );
    const headerText = wrapper.find("h1");
    const byLineText = wrapper.find(".source-text");
    const sourceText = wrapper.find(".byline");
    const dateLine = wrapper.find(".date-line");

    // console.log("text=====", byLineText);
    // console.log("text.text()=====", byLineText.text());

    expect(headerText.text()).toBe("HEADER");
    expect(byLineText.text()).toBe("BY LINE TEXT,");
    expect(sourceText.text()).toBe("SOURCE TEXT");
    expect(dateLine.text()).toBe(
      moment(new Date()).format("dddd, DD MMMM YYYY HH:mm")
    );
  });
});
