import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import ParagraphComponent from "../ParagraphComponent";
import moment from "moment";

Enzyme.configure({ adapter: new Adapter() });

describe("ParagraphComponent", () => {
  it("should be show Paragraph content", () => {
    const wrapper = shallow(
      <ParagraphComponent
        content={
          "That said-- you are getting an element by text, then verifying it contains the text you just used to query it, so that test might not be of very high value. It might make more sense to just verify that it is there:"
        }
      />
    );
    const content = wrapper.find("div p");

    // console.log("text=====", byLineText);
    // console.log("text.text()=====", byLineText.text());

    expect(content.text()).toBe(
      "That said-- you are getting an element by text, then verifying it contains the text you just used to query it, so that test might not be of very high value. It might make more sense to just verify that it is there:"
    );
  });
});
