import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import PullQuoteComponent from "../PullQuoteComponent";

Enzyme.configure({ adapter: new Adapter() });

describe("PullQuoteComponent", () => {
  it("should be show quoted text and attribution text", () => {
    const quoteValue =
      "That said-- you are getting an element by text, then verifying it contains the text you just used to query it, so that test might not be of very high value. It might make more sense to just verify that it is there:";
    const attribution = "Micheal Jackson";
    const wrapper = shallow(
      <PullQuoteComponent quoteObj={{ text: quoteValue, attribution }} />
    );
    const quoteText = wrapper.find(".quote-text");
    const attributionText = wrapper.find(".attribution-text");

    // console.log("text=====", byLineText);
    // console.log("text.text()=====", byLineText.text());

    expect(quoteText.text()).toBe(
      "That said-- you are getting an element by text, then verifying it contains the text you just used to query it, so that test might not be of very high value. It might make more sense to just verify that it is there:"
    );
    expect(attributionText.text()).toBe("Micheal Jackson");
  });
});
