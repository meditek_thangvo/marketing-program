import logo from "./logo.svg";
import { useState, useEffect } from "react";
import "./App.css";
import HeaderComponent from "./components/HeaderComponent";
import { JSONContent, moreContent } from "./codetestjson";
import ParagraphComponent from "./components/ParagraphComponent";
import ImageComponent from "./components/ImageComponent";
import PullQuoteComponent from "./components/PullQuoteComponent";
import ListComponent from "./components/ListComponent";

// console.log("content obj is", JSONContent);

const fakeApiLoadMore = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(moreContent);
    }, 1500);
  });
};

const fakeApiGetNews = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(JSONContent);
    }, 1000);
  });
};

function App() {
  const [loading, setLoading] = useState(false);
  const [news, setNews] = useState(null);

  const handleCallApiGetNews = async () => {
    const result = await fakeApiGetNews();
    setNews(result);
  };

  useEffect(() => {
    handleCallApiGetNews();
  }, []);

  const handleLoadMore = async () => {
    setLoading(true);
    const newResutl = await fakeApiLoadMore();
    console.log("newResutl is", newResutl);
    setNews(newResutl);
    setLoading(false);
    window.scrollTo({ top: document.body.scrollHeight, behavior: "smooth" });
  };

  return (
    <div className="app-container">
      {news ? (
        <div className="content">
          <HeaderComponent
            headerText={news.headline}
            sourceText={news.source}
            byLineText={news.byline}
            publicationDate={news.publicationDate}
          />
          <div className="div-blocks">
            {news.blocks.map((item, idx) => {
              if (item.kind === "text") {
                return <ParagraphComponent key={idx} content={item.text} />;
              } else if (item.kind === "image") {
                return <ImageComponent key={idx} image={item} />;
              } else if (item.kind === "pull-quote") {
                return <PullQuoteComponent key={idx} quoteObj={item} />;
              } else if (item.kind === "list") {
                return <ListComponent key={idx} list={item.items} />;
              }
            })}
          </div>
          <div className="load-more-div">
            <button
              id="loadMoreButton"
              className="button-loading"
              onClick={handleLoadMore}
            >
              {" "}
              {loading && <div class="loader"></div>}Load more
            </button>
          </div>
        </div>
      ) : (
        <div className="loader-page"></div>
      )}
    </div>
  );
}

export default App;
